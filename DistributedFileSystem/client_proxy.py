import sys
import json
import requests

#import os
from flask import Flask, request, send_from_directory
import requests

import base64

global client_callback_function
client_callback_function = None

global session_key, ticket

# Set the callback function for the client proxy to use with the locking system
def setCallbackFunction(function):
    global client_callback_function;
    client_callback_function = function;

# Configuration settings
UPLOAD_FOLDER = 'FileUploads'  # This path is relative - it is a folder located inside the project directory

PASSWORD_ENCRYPTION_KEY = 'abcdefg' # Known to both client proxy and directory server - used to encrypt newly-created passwords when they are sent to the server

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER  # Set location of upload folder
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # Set maximum file size

# Callback to the client that they have obtained the lock they are waiting for
@app.route('/notify_client/', methods=['POST'])
def notify_client():
    filename = decrypt_message(session_key, request.args['filename'])
    return client_callback_function(filename)

# Encrypt the given message using the given password as the key
def encrypt_message(password, message):
    encrypted_message = []
    for i in range(len(message)):
        password_char = password[i % len(password)]
        encrypted_char = chr((ord(message[i]) + ord(password_char)) % 256)
        encrypted_message.append(encrypted_char)
    return base64.urlsafe_b64encode("".join(encrypted_message).encode()).decode()

# Decrypt the given message using the given password as the key
def decrypt_message(password, encrypted_message):
    decrypted_message = []
    encrypted_message = base64.urlsafe_b64decode(encrypted_message).decode()
    for i in range(len(encrypted_message)):
        password_char = password[i % len(password)]
        decrypted_char = chr((256 + ord(encrypted_message[i]) - ord(password_char)) % 256)
        decrypted_message.append(decrypted_char)
    return "".join(decrypted_message)

# Authenticate user credentials
def authenticate_user(username, password):
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]

    encrypted_message = encrypt_message(password, 'LoginMessage')

    parameters= {'username': username, 'encrypted_message': encrypted_message}
    token = requests.get('http://' + directory_address + ':' + directory_port_number + '/authenticate_user/', params=parameters).text

    if token == 'AuthenticationFailure':  # Entered credentials weren't correct
        return token    # Client application will display this failure message to the user
    else:

        global session_key, ticket

        # Decrypt the token using the user's password
        token = decrypt_message(password, token)
        token_components = token.split(',')
        user_access_level = token_components[0]
        session_key = token_components[1]
        ticket = token_components[2]

        return user_access_level    # Client application will now alow user to access the system

# Get the list of user accounts on the system
def get_user_accounts():
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]

    parameters = {'ticket': ticket}
    response = requests.get('http://' + directory_address + ':' + directory_port_number + '/get_user_accounts/', params=parameters)
    return decrypt_message(session_key, response.text)

# Create a new user account
def create_user_account(username, password, access_level):
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]

    # Encrypt message parameters before sending it to the server
    username = encrypt_message(session_key, username)
    password = encrypt_message(session_key, password)
    access_level = encrypt_message(session_key, access_level)

    parameters = {'username': username, 'password': password, 'access_level': access_level, 'ticket': ticket}

    response = requests.post('http://' + directory_address + ':' + directory_port_number + '/create_user_account/', params=parameters)

# Delete an existing user account
def delete_user_account(username):
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]

    # Encrypt message parameters before sending it to the server
    username = encrypt_message(session_key, username)

    parameters = {'username': username, 'ticket': ticket}

    response = requests.post('http://' + directory_address + ':' + directory_port_number + '/delete_user_account/', params=parameters)

    return decrypt_message(session_key, response.text)

# Upload a file to the system
def uploadfile(filename, file_text):
    # Encrypt message parameters before sending it to the server
    filename = encrypt_message(session_key, filename)
    file_text = encrypt_message(session_key, file_text)

    files = {'file': (filename, file_text)}
    parameters = {'ticket': ticket}

    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    response = requests.post('http://' + directory_address + ':' + directory_port_number + '/directory_upload_file/', files=files, params = parameters)

# Get the names of all of the files in the system
def getfilenames():
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    parameters = {'ticket': ticket}
    response = requests.get('http://' + directory_address + ':' + directory_port_number + '/directory_get_file_names/', params=parameters).json()

    files = response['all_files']
    #filenumber = 0
    #for file in files:
    #    files[filenumber] = decrypt_message(session_key, files[filenumber])
    #    filenumber += 1
    return files

# Request the lock for a file on the system
def lock_file(filename, address, port_number):
    # Encrypt message parameters before sending it to the server
    filename = encrypt_message(session_key, filename)
    address = encrypt_message(session_key, address)
    port_number = encrypt_message(session_key, port_number)

    parameters = {'filename': filename, 'address': address, 'port_number': port_number, 'ticket': ticket}
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    response = requests.get('http://' + directory_address + ':' + directory_port_number + '/directory_lock_file/', params=parameters)

    return decrypt_message(session_key, response.text)


# Download a file from the system
def downloadfile(filename):
    # Encrypt message parameters before sending it to the server
    filename = encrypt_message(session_key, filename)
    parameters = {'filename': filename, 'ticket': ticket}
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    response = requests.get('http://' + directory_address + ':' + directory_port_number + '/directory_download_file/',  params=parameters)

    return decrypt_message(session_key, response.text)

# Check the version number of a file on the system
def get_version_number(filename):
    # Encrypt message parameters before sending it to the server
    filename = encrypt_message(session_key, filename)
    parameters = {'filename': filename, 'ticket': ticket}
    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    response = response = requests.get('http://' + directory_address + ':' + directory_port_number + '/directory_get_version/', params=parameters)

    # Return version number
    return decrypt_message(session_key, response.text)