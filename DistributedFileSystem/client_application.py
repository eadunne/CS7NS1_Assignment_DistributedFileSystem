import client_proxy

import sys
import base64

# Disable console messages from Flask
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

global file
file = None

user_access_level = None   # Level of access the user has in the system

global session_key, ticket
session_key = None
ticket = None

cached_files = []   # (filename, filetext, version_number)

def listserverfiles():
    filenames = client_proxy.getfilenames()
    if len(filenames) == 0:
        print('No files on server.')
    else:
        print('Files on server:')
        for filename in filenames:
            print(' ' + filename)

def open_file(filename):
    # Check the cache for a copy of this file
    cache_file_number = 0
    i = 0
    cached = False

    global cached_files
    for file in cached_files:  # Check to see if there is an old copy of this file in the cache
        if file[0] == filename:  # Found file in cache
            cached = True
            cache_file_number = i
        else:
            i += 1  # Try the next file in the cache

    server_version_response = client_proxy.get_version_number(filename)
    if server_version_response == 'File not in storage':
        print('The file you requested was not found on the system.')
        present_user_options()

    server_version_number = int(server_version_response)

    have_an_up_to_date_cach_version = False

    cache_file = None
    if cached:
        # Check the directory server to see if its version number is up-to-date
        cache_file = cached_files[cache_file_number]
        client_version_number = cache_file[2]

        if (client_version_number == server_version_number):  # If version number is up-to-date
            filetext = file[1]
            have_an_up_to_date_cach_version = True

    final_file = None
    if have_an_up_to_date_cach_version:
        final_file = cached_files[cache_file_number][1]
    if not have_an_up_to_date_cach_version:  # An up-to-date copy of the file was not found in the cache
        final_file = client_proxy.downloadfile(filename)  # Download the file from the server

    if final_file == 'File not in storage':
        print('The file you requested was not found on the system.')
        present_user_options()

    print('Opening file...')
    print('Contents of ' + filename + ':')
    print(final_file)

    # Get user edits
    new_file_text = input('Enter your edited version of this file\'s text:\n')

    # Save a cache copy of the file
    new_version_number = server_version_number + 1
    new_cached_file = (filename, new_file_text, new_version_number)

    if cached:
        cached_files[cache_file_number] = new_cached_file
    else:
        # Did not find a copy of this file in the cache
        cached_files.append(new_cached_file)

    # Upload the edited version of the file
    client_proxy.uploadfile(filename, new_file_text)

    print('Your changes have been uploaded to the server!')

    present_user_options()

# Decrypt the given message using the given password as the key
def decrypt_message(password, encrypted_message):
    decrypted_message = []
    encrypted_message = base64.urlsafe_b64decode(encrypted_message).decode()
    for i in range(len(encrypted_message)):
        password_char = password[i % len(password)]
        decrypted_char = chr((256 + ord(encrypted_message[i]) - ord(password_char)) % 256)
        decrypted_message.append(decrypted_char)
    return "".join(decrypted_message)

# Authenticate the user
def authenticate_user():
    logged_in = False
    while not logged_in:
        username = input('Please enter your username:\n')
        password = input('Please enter your password:\n')

        result = client_proxy.authenticate_user(username, password)

        if result == 'AuthenticationFailure':     # Entered credentials weren't correct
            print('Error: Invalid username and/or password. Please try again.')
        else:
            global user_access_level
            user_access_level = result   # User has been granted access to the system

            logged_in = True
            print('Login successful!')


def present_user_options():
    print('------------------')
    # List all of the files on the server
    listserverfiles()

    # List options
    print('\nOptions:')
    print(' Enter \'1\' to create a new file.')
    print(' Enter \'2\' to open an existing file on the server.')
    if user_access_level == 'Admin':    # Only admin-level users can create and delete user accounts
        print(' Enter \'3\' to manage user accounts.')
    print(' Enter \'x\' to quit.')

    user_choice = input('\nYour choice: ')

    # Choice 1: Create a new file
    if user_choice == '1':
        file_text = input('Enter your file text:\n')
        filename = input('Enter your file name: ')

        client_proxy.uploadfile(filename, file_text)

        # Save a cache copy of the file
        new_cached_file = (filename, file_text, 0)
        cached_files.append(new_cached_file)

        print ('Your file has been uploaded to the server!')
        present_user_options()

    # Choice 2: Open an existing file
    elif user_choice == '2':
        filename = input('Enter the name of the file you want to open: ' )
        print('Retrieving your file...')

        # Request that the directory lock the file
        address = sys.argv[1]
        port_number = sys.argv[2]
        lock_response = client_proxy.lock_file(filename, address, port_number)

        if lock_response == 'Wait for file to be unlocked': # If the file is locked by another client already
            print('The file you requested is currently locked for editing by another client. Please wait.')
            address = sys.argv[1]
            port_number = sys.argv[2]
            client_proxy.app.run(debug=False,host=address, port=int(port_number))  # Make the client proxy listen for a response from the server
                                                                                    # The client proxy will make a callback to the client when the file is made available
        else:
            open_file(filename)
            present_user_options()

    # Choice 3: Manage user accounts (user must have admin-level access)
    elif user_choice == '3' and user_access_level == 'Admin':
        present_account_control_options()

    elif user_choice == 'x':    # Quit
        print('Shutting down...')
        sys.exit()

    else:
        print('Invalid input received.')
        present_user_options()

def present_account_control_options():
    print('------------------')
    text_user_accounts = client_proxy.get_user_accounts()
    user_accounts = text_user_accounts.split(',')

    print('User accounts on system:')
    for user_account in user_accounts:
        print(' ' + user_account)

    print(' Enter \'1\' to create a new user account.')
    print(' Enter \'2\' to delete an existing user account.')
    print(' Enter \'x\' to return to the previous menu.')

    user_choice = input('\nYour choice: ')

    if (user_choice == '1'):  # Create a new user account
        new_username = input('Enter a username: ')
        new_username = new_username.replace(',', '')    # Remove any commas
        new_password = input('Enter a password: ')
        access_level_choice = input('Give this user administrative access? (Y/N): ')

        if access_level_choice.upper() == 'Y':
            new_access_level = 'Admin'
        else:
            new_access_level = 'User'

        client_proxy.create_user_account(new_username, new_password, new_access_level)
        print('New user account created!')
        present_account_control_options()

    elif (user_choice == '2'):  # Delete an existing user account
        username_to_delete = input('Enter the username of the account you wish to delete: ')

        response = client_proxy.delete_user_account(username_to_delete)

        print(response)  # Print result
        present_account_control_options()

    elif (user_choice == 'x'):  # Return to the previous menu
        present_user_options()

    else:
        print('Invalid input received.')
        present_account_control_options()

###################################################################
# Startup procedure
if sys.argv.__len__() < 5:
    print('You have not entered enough parameters. Please follow this format:')
    print('python client_application.py <client application\'s IP address> <client application\'s port number>  <directory server\'s IP address> <directory server\'s port number>')
    print('See the README file for more information.')
    sys.exit()

client_proxy.setCallbackFunction(open_file)
authenticate_user()
present_user_options()






