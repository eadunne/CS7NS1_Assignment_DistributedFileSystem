The Distributed File System project consists of four applications: directory server, file server, client proxy (a library) and client application.


Begin the system by launching the directory server. The command line arguments needed to launch the directory server are as follows:
python directory_server.py [directory server's IP address] [directory server's port number] 

For example:
python directory_server.py localhost 1000


At least one file server should then be launched. The command line arguments needed to launch the file server are as follows:
python file_server.py [file server's IP address] [file server's port number]  [directory server's IP address] [directory server's port number] 

For example:
python file_server.py localhost 2000 localhost 1000

NB) Each file server provides access to all files in a folder named 'FileUploads' within the same directory as itself. If this folder does not exist when the file server
is launched, it will create it itself.


A client application should then be started. The command line arguments needed to launch the client application are as follows:
python client_application.py [client application's IP address] [client application's port number]  [directory server's IP address] [directory server's port number]

For example:
python client_application.py localhost 3000 localhost 1000 


