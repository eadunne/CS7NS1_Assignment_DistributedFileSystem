import os
from flask import Flask, request, send_from_directory
from werkzeug.utils import secure_filename
import json
import requests
import threading
import sys

import base64

import random
import string

# Configuration settings
UPLOAD_FOLDER = 'FileUploads'   # This path is relative - it is a folder located inside the project directory
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
USER_ACCOUNTS_FOLDER = 'UserAccounts'   # Storage for user security accounts
USER_ACCOUNTS_FILE = 'UserAccounts.txt'

SERVER_ENCRYPTION_KEY = 'abcdefg'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER # Set location of upload folder
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024 # Set maximum file size
app.config['USER_ACCOUNTS_FOLDER'] = USER_ACCOUNTS_FOLDER
app.config['USER_ACCOUNTS_FILE'] = USER_ACCOUNTS_FILE

# File servers
server_ports = []
server_addresses = []
global server_files
server_files = []   # (filename, lock, version_number, [lock_queue])
number_of_file_servers = 0
next_file_server = 0

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class myThread (threading.Thread):
   def __init__(self, threadID, name, counter, client_address, client_port_number, client_ticket, filename):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
      self.client_address = client_address
      self.client_port_number = client_port_number
      self.client_ticket = client_ticket
      self.filename = filename
   def run(self):
      notify_client(self.client_address, self.client_port_number, self.client_ticket, self.filename)

# Upload a file to one of the available file servers
@app.route('/directory_upload_file/', methods=['POST'])
def upload_file():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    f = request.files['file']
    filename = decrypt_message(session_key, f.filename)
    filetext = decrypt_message(session_key, f.read())
    files = {'file': (filename, filetext)}
    global server_files

    test = f.read()

    # Determine which file server this file should be uploaded to
    server_number = 0
    for server in server_ports:  # For every available file server
        file_number = 0

        for file in server_files[server_number]:  # For every file on the file server
            if file[0] == filename:  # Found the file - file[0] is the filename, file[1] is the lock
                port_number = server_ports[server_number]
                address = server_addresses[server_number]
                version_number = file[2] + 1    # Increment the version number
                # Get the locking queue
                existing_locking_queue = file[3]

                client_waiting = False
                lock_status = None
                next_client_to_get_the_lock = None
                if len(existing_locking_queue) > 0: # If there is at least one client waiting for the lock
                    client_waiting = True
                    lock_status = 1 # File remains locked
                    next_client_to_get_the_lock = existing_locking_queue[0]
                    del existing_locking_queue[0]   # Remove the client at the head from the list
                else:
                    lock_status = 0 # File becomes unlocked


                file = (filename, lock_status, int(version_number), existing_locking_queue)
                server_files[server_number][file_number] = file
                response = requests.post('http://' + address + ':' + port_number + '/upload_file/', files=files)

                # If there is at least one client waiting to obtain the lock, inform them now that the lock is free
                if client_waiting:
                    thread1 = myThread(1, "Thread-1", 1, client_address=next_client_to_get_the_lock[0], client_port_number=next_client_to_get_the_lock[1], client_ticket=next_client_to_get_the_lock[2], filename=filename)
                    thread1.start()

                return encrypt_message(session_key, 'Directory: replacement version of file uploaded')

            file_number += 1


        server_number += 1  # Try the next file server

    # If this point is reached, the file was not already in storage


    global next_file_server
    port_number = server_ports[next_file_server]
    address = server_addresses[next_file_server]

    response = requests.post('http://' + address + ':' + port_number + '/upload_file/', files=files)
    # Brand new file - mark the file as unlocked, with an empty locking queue
    newfile = (filename, 0, 1, [])
    server_files[next_file_server].append(newfile)

    next_file_server += 1
    if next_file_server == number_of_file_servers:
        next_file_server = 0

    return encrypt_message(session_key, 'Directory: new file uploaded')

# Notify a client that they have obtained the lock they are waiting for
def notify_client(client_address, client_port_number, client_ticket, filename):
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, client_ticket)

    filename = encrypt_message(session_key, filename)
    parameters = {'filename': filename}
    response = requests.post('http://' + client_address + ':' + client_port_number + '/notify_client/', params=parameters)

# Get the name of every file on every available file server
@app.route('/directory_get_file_names/', methods=['GET'])
def get_file_names():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    all_files = []

    server_number = 0
    for server_port in server_ports:
        address = server_addresses[server_number]
        response = requests.get('http://' + address + ':' + server_port + '/get_file_names/').json()
        files_on_server = response['files']

        for file in files_on_server:
            all_files.append(file)

        server_number += 1

    result = {'all_files': all_files}
    #return encrypt_message(session_key, json.dumps(result))
    return json.dumps(result)

# Request to obtain the lock for a file
@app.route('/directory_lock_file/', methods=['GET'])
def lock_file():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    # Find the file server that has the requested file
    global server_files

    filename = decrypt_message(session_key, request.args.get('filename'))

    server_number = 0
    for server in server_ports:  # For every available file server
        file_number = 0
        for file in server_files[server_number]:  # For every file on the file server
            if file[0] == filename:  # Found the file - file[0] is the filename, file[1] is the lock, file[2] is the version number, file[3] is the lock queue

                if (file[1] == 1):  # If the file is locked
                    # Put this client in the queue, so they will be notified when the lock is free
                    existing_lock_queue = file[3]
                    port_number = decrypt_message(session_key, request.args.get('port_number'))
                    address = decrypt_message(session_key, request.args.get('address'))
                    new_entry_to_lock_queue = (address, port_number, ticket)
                    existing_lock_queue.append(new_entry_to_lock_queue)
                    new_file = (file[0], file[1], file[2], existing_lock_queue)
                    # Update records
                    server_files[server_number][file_number] = new_file
                    return encrypt_message(session_key, 'Wait for file to be unlocked')
                else:
                    new_file = (file[0], 1, file[2], file[3])  # Lock the file
                    server_files[server_number][file_number] = new_file
                    return encrypt_message(session_key, 'You now have the lock')

            file_number += 1
        server_number += 1

    # If this point is reached, the requested file was not found on any of the file servers
    return encrypt_message(session_key, 'File not in storage')

# Get the version of a file on one of the file servers
@app.route('/directory_get_version/', methods=['GET'])
def get_version():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    # Find the file server that has the requested file
    global server_files

    filename = decrypt_message(session_key, request.args.get('filename'))

    server_number = 0
    for server in server_ports: # For every available file server
        file_number = 0
        for file in server_files[server_number]:    # For every file on the file server
            if file[0] == filename:    # Found the file - file[0] is the filename, file[1] is the lock, file[2] is the version number
                version_number = file[2]
                return encrypt_message(session_key, str(version_number)) # Return the version number


            file_number += 1
        server_number += 1

    # If this point is reached, the requested file was not found on any of the file servers
    return encrypt_message(session_key, 'File not in storage')

# Open a file on one of the file servers
@app.route('/directory_download_file/', methods=['GET'])
def download_file():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    filename = decrypt_message(session_key, request.args.get('filename'))

    global server_files

    server_number = 0
    for server in server_ports: # For every available file server
        file_number = 0
        for file in server_files[server_number]:    # For every file on the file server
            if file[0] == filename:    # Found the file - file[0] is the filename, file[1] is the lock

                port_number = server_ports[server_number]
                address = server_addresses[server_number]
                files = {'file': (filename, 'text')}
                response = requests.get('http://' + address + ':' + port_number + '/open_file/', files=files)

                version_number = file[2]
                #file = (filename, 1, version_number)  # Lock the file
                #server_files[server_number][file_number] = file

                return encrypt_message(session_key, response.text)

            file_number += 1    # Try the next file on the server

        server_number += 1  # Try the next file server

    # If this point is reached, the requested file was not found on any of the file servers
    return encrypt_message(session_key, 'File not in storage')

# Add a new file server to the system
@app.route('/add_file_server/', methods=['POST'])
def add_file_server():

    global server_files

    port_number = decrypt_message(SERVER_ENCRYPTION_KEY, request.args.get('port_number'))
    address = decrypt_message(SERVER_ENCRYPTION_KEY, request.args.get('address'))

    # Get list of files on the new file server
    filenamestring = decrypt_message(SERVER_ENCRYPTION_KEY, request.args.get('filenamestring'))
    filenames = filenamestring.split('/')
    filenames.remove('')    # Remove blank entry from list

    newfiles = []
    for filename in filenames:
        newfile = (filename, 0, 0, [])  # New files from the newly-added file server
        newfiles.append(newfile)

    # Add the new server to the list of servers if it is not already in the list
    if not port_number in server_ports:
        server_ports.append(port_number)
        server_addresses.append(address)
        server_files.append(newfiles)
        global number_of_file_servers
        number_of_file_servers += 1

    return encrypt_message(SERVER_ENCRYPTION_KEY, 'Added file server')

# Decrypt the given message using the given password as the key
def decrypt_message(password, encrypted_message):
    decrypted_message = []
    encrypted_message = base64.urlsafe_b64decode(encrypted_message).decode()
    for i in range(len(encrypted_message)):
        password_char = password[i % len(password)]
        decrypted_char = chr((256 + ord(encrypted_message[i]) - ord(password_char)) % 256)
        decrypted_message.append(decrypted_char)
    return "".join(decrypted_message)

# Encrypt the given message using the given password as the key
def encrypt_message(password, message):
    encrypted_message = []
    for i in range(len(message)):
        password_char = password[i % len(password)]
        encrypted_char = chr((ord(message[i]) + ord(password_char)) % 256)
        encrypted_message.append(encrypted_char)
    return base64.urlsafe_b64encode("".join(encrypted_message).encode()).decode()

# Authenticate a user's credentials
@app.route('/authenticate_user/', methods=['GET'])
def authenticate_user():
    # Should return token
    # Token contains ticket and session key
    # Session key generated randomly

    username = request.args.get('username')
    encrypted_message = request.args.get('encrypted_message')

    accounts_file = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "r")
    accounts_file_lines = accounts_file.readlines()

    for account in accounts_file_lines:
        account = account.split('\n')[0]    # Remove trailing newline character
        components = account.split(',')
        username_on_file = components[0]

        if username == username_on_file:
            password_on_file = components[1]
            # Now decrypt the encrypted message using the password on file for this usernames

            decrypted_message = decrypt_message(password_on_file, encrypted_message)
            if decrypted_message == 'LoginMessage':

                session_key = ''.join(random.choices(string.ascii_letters + string.digits, k=6))    # Session key is a 6-character long randomly-generated string
                ticket = encrypt_message(SERVER_ENCRYPTION_KEY, session_key)    # Session key is encrypted with server encryption key and put into ticket

                token = components[2] + ',' + session_key + ',' + ticket   # Login was successful - return token containing access level, session key and ticket
                token = encrypt_message(password_on_file, token)    # Encrypt the token using the password given by the client
                return token    # Return token to the client
            else:
                return 'AuthenticationFailure'

    return 'AuthenticationFailure'

# Get the list of user accounts on the system
@app.route('/get_user_accounts/', methods=['GET'])
def get_user_accounts():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    accounts_file = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "r")
    accounts_file_lines = accounts_file.readlines()

    response_text = ''

    for account in accounts_file_lines:
        components = account.split(',')
        username_on_file = components[0]
        response_text += username_on_file + ','

    return encrypt_message(session_key, response_text)    # Return comma-separated list of usernames

# Create a new user account
@app.route('/create_user_account/', methods=['POST'])
def create_user_account():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    username = decrypt_message(session_key, request.args.get('username'))
    password = decrypt_message(session_key, request.args.get('password'))
    access_level = decrypt_message(session_key, request.args.get('access_level'))


    accounts_file = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "a")  # Append to file
    accounts_file.write('\n' + username + ',' + password + ',' + access_level)

    return encrypt_message(session_key, 'User account created')

# Delete an existing user account
@app.route('/delete_user_account/', methods=['POST'])
def delete_user_account():
    ticket = request.args.get('ticket')
    session_key = decrypt_message(SERVER_ENCRYPTION_KEY, ticket)

    username = decrypt_message(session_key, request.args.get('username'))

    accounts_file = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "r")  # Read-only
    accounts_file_lines = accounts_file.readlines()
    accounts_file.close()

    deleted_account = False
    no_accounts_left = True

    accounts_file = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "w")  # Overwrite

    for account in accounts_file_lines:
        account = account.split('\n')[0]    # Remove trailing newline character
        components = account.split(',')
        username_on_file = components[0]
        if username_on_file == username:    # Found user to delete
            deleted_account = True
        else:
            if not no_accounts_left:  # If not first account in the list
                accounts_file.write('\n')
            accounts_file.write(account)
            no_accounts_left = False

    if no_accounts_left:    # If all accounts are deleted, re-instate the default user profile, so that the system can still be accessed
        accounts_file.write('default_user,password,Admin')

    accounts_file.close()

    if deleted_account:
        return encrypt_message(session_key, 'User account deleted.')
    else:
        return encrypt_message(session_key, 'Error: User not found on system.')


###################################
# Startup procedure
if sys.argv.__len__() < 3:
    print('You have not entered enough parameters. Please follow this format:')
    print('python directory_server.py <directory server\'s IP address> <directory server\'s port number>')
    print('See the README file for more information.')
    sys.exit()

# Check for stored user security accounts
# Create the user accounts folder if it doesn't already exist
if not os.path.exists(app.config['USER_ACCOUNTS_FOLDER']):
    os.makedirs(app.config['USER_ACCOUNTS_FOLDER'])

# Create the user accounts file if it doesn't already exist
if not os.path.isfile(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE):
    f = open(USER_ACCOUNTS_FOLDER + '/' + USER_ACCOUNTS_FILE, "w+")
    f.write('default_user,password,Admin')    # Add default credentials, so that it is still possible for the very first user of the system to log on
    f.close()

if __name__ == '__main__':
    # app.run(debug=True)
    address = sys.argv[1]
    port_number = sys.argv[2]
    app.run(debug=False, host=address, port=int(port_number))