#import os
from flask import Flask, request, send_from_directory
#from werkzeug.utils import secure_filename
#import json
import requests
import sys
import json
import os
from werkzeug.utils import secure_filename

import base64

# Configuration settings
UPLOAD_FOLDER = 'FileUploads'  # This path is relative - it is a folder located inside the project directory
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

SERVER_ENCRYPTION_KEY = 'abcdefg'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER  # Set location of upload folder
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # Set maximum file size

# Encrypt the given message using the given password as the key
def encrypt_message(password, message):
    encrypted_message = []
    for i in range(len(message)):
        password_char = password[i % len(password)]
        encrypted_char = chr((ord(message[i]) + ord(password_char)) % 256)
        encrypted_message.append(encrypted_char)
    return base64.urlsafe_b64encode("".join(encrypted_message).encode()).decode()

# Get the names of files on the server
@app.route('/get_file_names/', methods=['GET'])
def get_file_names():
    files = os.listdir(app.config['UPLOAD_FOLDER'])
    result = {'files': files}
    return json.dumps(result)

# Open a file on the server
@app.route('/open_file/', methods=['GET'])
def open_file():
    f = request.files['file']
    filepath = app.config['UPLOAD_FOLDER']
    return send_from_directory(directory = filepath, filename = secure_filename(f.filename))

# Upload a file to the server
@app.route('/upload_file/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        filepath = app.config['UPLOAD_FOLDER'] + '/' + secure_filename(f.filename)
        f.save(filepath)

        return 'file uploaded successfully'

###################################
# Startup procedure
if sys.argv.__len__() < 5:
    print('You have not entered enough parameters. Please follow this format:')
    print('python file_server.py <file server\'s IP address> <file server\'s port number>  <directory server\'s IP address> <directory server\'s port number>')
    print('See the README file for more information.')
    sys.exit()

# Establish contact with the directory server
address = sys.argv[1]
port_number = sys.argv[2]

# Create the file upload folder if it doesn't already exist
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])

# Get any existing files
filenames = os.listdir(app.config['UPLOAD_FOLDER'])

filenamestring = ''
for filename in filenames:
    filenamestring = filenamestring + filename + '/'

filenamestring = encrypt_message(SERVER_ENCRYPTION_KEY, filenamestring)
port_number = encrypt_message(SERVER_ENCRYPTION_KEY, port_number)
address = encrypt_message(SERVER_ENCRYPTION_KEY, address)
parameters = {'port_number': port_number, 'address': address, 'filenamestring': filenamestring }

# Establish contact with the directory server
directory_address = sys.argv[3]
directory_port_number = sys.argv[4]
response = requests.post('http://' + directory_address + ':' + directory_port_number + '/add_file_server/', params=parameters)
print('Connection to the directory server established.')



if __name__ == '__main__':
    address = sys.argv[1]
    port_number = sys.argv[2]
    app.run(debug=False, host=address, port=int(port_number))